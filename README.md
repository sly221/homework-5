# Lehigh University CSE262 - Programming Languages - Homework 5

Solve the following ten questions regarding the Lambda Calculus.

## Question 1

Make all parentheses explicit in these λ- expressions:

1. ((λp.pz) (λq.(w (λw.(wqzp)))))


2. (λp.(pq (λp.(qp))))


## Question 2

In the following expressions say which, if any, variables are bound (and to which λ), and which are free.

1. (λs.(sz(λq(.sq))))
    The s in λs.sz is bound to s, s following λq is bound and the q is bound to λq. The z following the λs is free.

2. (λs.sz)(λq.(wλw.(wqzs)))
    The s in λs.sz is bound to λs, the q is bound to the λq, and the w in λw.(wqzs) is bound to λw. The rest of the variables are free.

3. (λs.s) (λq.qs)
    The s in (λs.s) is bound to λs and the q is bound to λq. The s in (λq.qs) is free.

4. λz. (((λs.sq) (λq.qz)) λz. (z z))
    The s in (λs.sq) is bound to the λs, the q in (λq.qz) is bound to the λq, both z's are bound to the second λz, and the z in λq.qz is bound to the first λz. All of the other variables are free.

## Question 3

Put the following expressions into beta normal form (use β-reduction as far as possible, α-conversion as needed) assuming left-association.

1. (λz.z) (λq.q q) (λs.s a)
    (z)[z->λq.q q] (λs.s a)
    (λq.q q) (λs.s a)
    (q q) [q->λs.s a)
    (λs.s a) (λs.s a)
    (s a) [s->λs.s a]
    ((λs.s a) a) [s->a]
    (a a)

2. (λz.z) (λz.z z) (λz.z q)
    (λz.z) (λz.z z) (λz.z q)
    (z) (z->λz.z z) (λz.z q)
    (λz.z z) (λz.z q)
    (z z) (z->λz.z q)
    (λz.z q) (λz.z q)
    (z q) [z->λz.z q]
    ((λz.z q) q) [z->q]
    (q q)

3. (λs.λq.s q q) (λa.a) b
    (λq.s q q) [s->λa.a] b
    (λq.(λa.a)q q) b
    ((λa.a) q q) [q->b]
    (λa.a) b b
    (a) [a->b] b
    (b b)

4. (λs.λq.s q q) (λq.q) q
    (λq.s q q) (s->λq.q') q
    (λq.(λq.q') q q) q
    ((λq.q') q q) [q->q']
    ((λq.q') q' q')
    (q') [q'->q'] q
    (q' q')

5. ((λs.s s) (λq.q)) (λq.q)
    (s s) [s->λq.q] (λq.q)
    (λq.q) (λq.q) (λq.q)
    (q) [q->λq.q] (λq.q)
    (λq.q) (λq.q)
    (q) [q->λq.q]
    (λq.q)

## Question 4

1. Write the truth table for the or operator below.
OR
T T T
T F T
F T T
F F F

T = (λpλq.p)
F = (λpλq.q)
OR = (λp.λq.p p q)

2. The Church encoding for OR = (λp.λq.p p q)

Prove that this is a valid "or" function by showing that its output matches the truth table above. You will have 4 derivations. For the first derivation, show the long-hand solution (don't use T and F, use their definitions). For the other 3 you may use the symbols in place of the definitions. 

OR TT:
(λp.λq.p p q) (λpλq.p) (λpλq.p)
(λq.p p q) [p->λpλq.p] (λpλq.p)
(λq.(λpλq.p) (λpλq.p) (q)) (λpλq.p)
(λpλq.p) (λpλq.p) (q) [q->λpλq.p]
(λpλq.p) (λpλq.p) (λpλq.p)
(λq.p) [p->λpλq.p] (λpλq.p)
(λq.(λpλq.p)) (λpλq.p)
(λpλq.p) [q->λpλq.p]
(λpλq.p) = T

OR TF:
(λp.λq.p p q)TF
(λq.p p q)[p->T]F
(λq.T T q)F
(T T q) [q->F]
(T T F) = T

OR FT
(λp.λq.p p q)FT
(λq.p p q)[p->F]T
(λq.F F q)T
(F F q) [q->T]
(F F T) = T

OR FF
(λp.λq.p p q)FF
(λq.p p q)[p->F]F
(λq.F F q)F
(F F q) [q->F]
(F F F) = F

## Question 5

Derive a lambda expression for the NOT operator. Explain how this is similar to an IF statement.

T F 
F T 

NOT = (λa, a F T)

(λa, a F T) T
(a F T) [a->T]
(T F T) = F

(λa, a F T) F
(a F T) [a->F]
(F F T) = T

This is similar to an IF statement because a not statement is saying if not false then true and an if statement says the same thing, if the value is not false then it is true. It is the opposite for an else.

## Instructions

1. Fork the relevant repository into your own namespace. [Instructions](https://docs.gitlab.com/ee/workflow/forking_workflow.html#creating-a-fork)
2. Set your forked repository visibility to private. [Instructions](https://docs.gitlab.com/ee/public_access/public_access.html#how-to-change-project-visibility)
3. Add user "LehighCSE262" as a member to the project with "maintainer" access-level. [Instructions](https://docs.gitlab.com/ee/user/project/members/#add-a-user). 
4. Clone your newly forked repository. [Instructions](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html#clone-a-repository) 
5. Answer the questions here in the readme or in another document. Upload your solutions here to Gitlab.